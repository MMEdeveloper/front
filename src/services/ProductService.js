import axios from 'axios';

const apiClient = axios.create({
    baseURL: 'http://localhost:8000/api',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-type': 'application/json'
    }
})

export default {
    getSuppliers()
    {
        return apiClient.get('/suppliers')
    },
    getProducts()
    {
        return apiClient.get('/products')
    },
    getMaterials()
    {
        return apiClient.get('/materials')
    },
    postOrderItem(order)
    {
        return apiClient.post('/ordered-item', order)
    },
    postOrder()
    {
        return apiClient.post('/order')
    }
}